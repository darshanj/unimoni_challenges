package com.example.eurekclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EurekaRegistryCleintApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaRegistryCleintApplication.class, args);
	}

}

