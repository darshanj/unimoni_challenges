package com.example.eurekclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EurekaRegistryUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaRegistryUserApplication.class, args);
	}

}

