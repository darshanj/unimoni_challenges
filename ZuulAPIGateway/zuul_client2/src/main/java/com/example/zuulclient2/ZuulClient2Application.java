package com.example.zuulclient2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZuulClient2Application {

	public static void main(String[] args) {
		SpringApplication.run(ZuulClient2Application.class, args);
	}

}

